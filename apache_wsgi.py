import site
site.addsitedir('/home/clusterfudge/.virtualenvs/bungie-stats/lib/python2.7/site-packages')
site.addsitedir('/home/clusterfudge/bungie-stats')
import newrelic.agent
newrelic.agent.initialize('/home/clusterfudge/bungie-stats/newrelic.ini', 'production')
import wsgi
from wsgi import application
