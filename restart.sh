#!/bin/bash
TOP=$(cd $(dirname $0) && pwd)
PID="$(ps ax | grep -i cherryd | grep -v grep)"

if [ "$PID" = "" ]; then
  echo "Restarting cherrypy at $(date)"
  cd $TOP
  ./start.sh
fi
