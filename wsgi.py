import os
import cherrypy
from controllers.app import Controller

PATH = os.path.abspath(os.path.dirname(__file__))

cherrypy.tree.mount(Controller(), '/', config={
        '/': {
                'tools.staticdir.on': True,
                'tools.staticdir.dir': PATH + '/views',
                'tools.staticdir.index': 'index.html',
            },
    })

cherrypy.config.update({
    "server.socket_host": "0.0.0.0",
    "server.socket_port": 8080,
    })

def application(env, start_response):
    return cherrypy.tree(env, start_response)

if __name__ == '__main__':
    cherrypy.engine.start()
