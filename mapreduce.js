var map = function() {
  var columns = 'gameMap'.split(',');
  var summation = 'kills';
  var increment = 'month';
  var colVals = [];
  for (var i = 0; i < columns.length; i++) {
   if (this[columns[i]] == undefined) return;
    colVals.push(this[columns[i]]);
  }
  var summation_value = typeof(this[summation]) == 'string' ? 1 : this[summation];
  var series_name = colVals.join('-');
  var emitMe = {};
  emitMe = {};
  emitMe[this[increment]] = summation_value;
  emit(series_name, emitMe);
  var emitTS = {};
  emitTS[this[increment]] = this['gameId'];
  emit('timestamps', emitTS);
}


var reduce = function(key, values) {
  if (key == 'timestamps') {
    //get the lowest gameId for each timestamp
    var result = {};
    values.forEach(function (value) {
      for (var k in value) {
        if (!result[k] || result[k] > value[k]) {
          result[k] = value[k];
        }
      }
    });
    return result;
  } else {
    //aggregate the series data
    var result = {};
    values.forEach(function(value) {
      for (var key in value) {
        var val = value[key];
        if (result[key]) {
          val += result[key];
        }
        result[key] = val;
      }
    });
    return result;
  }
}


db.getCollection('stats-large').mapReduce(map, reduce, {out: 'mapout'})
