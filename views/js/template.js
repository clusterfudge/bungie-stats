// Simple JavaScript Templating
// John Resig - http://ejohn.org/ - MIT Licensed
(function(){
  var cache = {};

  this.tmpl = function tmpl(str, data){
    // Figure out if we're getting a template, or if we need to
    // load the template - and be sure to cache the result.
    var fn = !/\W/.test(str) ?
      cache[str] = cache[str] ||
        tmpl(document.getElementById(str).innerHTML) :

      // Generate a reusable function that will serve as a template
      // generator (and which will be cached).
      new Function("obj",
        "var p=[],print=function(){p.push.apply(p,arguments);};" +

        // Introduce the data as local variables using with(){}
        "with(obj){p.push('" +

        // Convert the template into pure JavaScript
        str
          .replace(/[\r\t\n]/g, " ")
          .split("<%").join("\t")
          .replace(/((^|%>)[^\t]*)'/g, "$1\r")
          .replace(/\t=(.*?)%>/g, "',$1,'")
          .split("\t").join("');")
          .split("%>").join("p.push('")
          .split("\r").join("\\'")
      + "');}return p.join('');");

    // Provide some basic currying to the user
    return data ? fn( data ) : fn;
  };
})();

var templates = {};

function render(template_path, args, element) {
	if (template_path in templates) {
	    if (args) {
            var html = templates[template_path](args);
            if (element) {
                jQuery(element).html(html);
            } 
            return html;
        } else {
            return templates[template_path];
        }
	} else {
        jQuery.ajax({
            url: template_path,
            async: element != null,
            cache: false,
            contentType: 'text/html',
            success: function(data) {
                templates[template_path] = tmpl(data);
                if (args && element) {
                    var html = templates[template_path](args);
                    jQuery(element).html(html);
                }
            }
        });
        if (args && !element) {
            var html = templates[template_path](args);
            return html;
        } else if (!element) {
            return templates[template_path];
        }
    }
}

function preload() {
    for (arg in arguments) {
        try {
            render(arguments[arg], null, null, true);
        } catch (e) {
            console.log(e);
        }
    }
}