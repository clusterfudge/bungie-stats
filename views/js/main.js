var graphs = {};
graphs['kills-player-month'] = {
        url: 'graph?increment=month',
        title: 'Kills per Player Per Month',
        type: 'line'
}

graphs['kills-map-month'] = {
        url: 'graph?series_member=gameMap&increment=month',
        title: 'Kills per Map per Month',
        type: 'line'
}

graphs['kills-gameType-month'] = {
        url: 'graph?series_member=gameType&increment=month',
        title: 'Kills per Game Type per Month',
        type: 'line'
}

graphs['kills-player-day'] = {
        url: 'graph?increment=day',
        title: 'Kills per Player Per Month',
        type: 'line'
}

graphs['kills-map-day'] = {
        url: 'graph?series_member=gameMap&increment=day',
        title: 'Kills per Map per Month',
        type: 'line'
}

graphs['kills-gameType-day'] = {
        url: 'graph?series_member=gameType&increment=day',
        title: 'Kills per Game Type per Month',
        type: 'line'
}


function init() {
    render('header.html', {}, $('#header'));
    fetchGraph('kills-player-month');
    $('#dialog').dialog({
        autoOpen: false,
    });
}

function about() {
    $('#dialog').dialog('open');
}

function createGraphFromForm() {
    var series_options = $('#series_options').val();
    var series_aggregation = $('#series_aggregation').val();
    var increment = $('#increment').val();
    var url = 'graph?series_member=' + series_options + '&series_column=' + series_aggregation + '&increment=' + increment;
    var filters = [];
    var filterLines = $('#filters').val().split(/[,\n]/);
    for (var i = 0; i < filterLines.length; i++) {
        var line = filterLines[i].replace(' = ', '=').trim().replace(' ','+');
        filters.push(line);
    }
    var graphType = $('#graphType').val();
    url += '&' + filters.join('&');
    var dataset = $('#dataset').val();
    url += '&collection=' + dataset;
    doFetchGraph({
        url:url,
        title: 'Custom Graph',
        type: graphType
        
    });
}

function fetchGraph(graph_name) {
    var graph = graphs[graph_name]
    doFetchGraph(graph);
}

function doFetchGraph(graph) {
    $('#container').html('');
    $('#container').css('width', (window.innerWidth - 250) + "px");
    $('#container').css('height', (window.innerHeight - 70) + "px");
    
    $.ajax({
        url: graph.url,
        cache: false,
        dataType: 'json',
        success: function(data) {
            buildGraph(graph.title, data['axis-label'], data['serieses'], data.min, data.max, graph.type);
        },
        error: function(jqXHR, textStatus, errorThrown) {
          alert('Sorry, but the server encountered an error');
        }
    })
}
function buildGraph(graph_title, categories, series, min, max, type) {
$(function () {
    var chart;
    $(document).ready(function() {
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'container',
                type: type,
                marginRight: 200,
                marginBottom: 20
            },
            title: {
                text: graph_title,
                x: -20 //center
            },
            subtitle: {
                text: 'Source: Bungie.net',
                x: -20
            },
            xAxis: {
                categories:categories
            },
            yAxis: {
                title: {
                    text: 'Count'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                formatter: function() {
                        return '<b>'+ this.series.name +'</b><br/>'+
                        this.x +': '+ this.y;
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'top',
                x: -10,
                y: max,
                borderWidth: 0
            },
            series: series
        });
    });
    
});
}
