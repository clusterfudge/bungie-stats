# install dependencies (*nix systems)
```
for i in $(cat requirements.txt); do
  pip install $i
done
```
# Running the Code
```
cherryd -i wsgi
```
