function() {
  var columns = '%%COLUMNS%%'.split(',');
  var summation = '%%SUMMATION%%';
  var increment = '%%INCREMENT%%';
  var colVals = [];
  for (var i = 0; i < columns.length; i++) {
   if (this[columns[i]] == undefined) return;
    colVals.push(this[columns[i]]);
  }
  var summation_value = typeof(this[summation]) == 'string' ? 1 : this[summation];
  var series_name = colVals.join('-');
  var emitMe = {};
  emitMe = {};
  emitMe[this[increment]] = summation_value;
  emit(series_name, emitMe);
  var emitTS = {};
  emitTS[this[increment]] = this['gameId'];
  emit('timestamps', emitTS);
}
