function(key, values) {
  if (key == 'timestamps') {
    //get the lowest gameId for each timestamp
    var result = {};
    values.forEach(function (value) {
      for (var k in value) {
        if (!result[k] || result[k] > value[k]) {
          result[k] = value[k];
        }
      }
    });
    return result;
  } else {
    //aggregate the series data
    var result = {};
    values.forEach(function(value) {
      for (var key in value) {
        var val = value[key];
        if (result[key]) {
          val += result[key];
        }
        result[key] = val;
      }
    });
    return result;
  }
}
