from models.db import getCollection
from models.db import getMapReduceFunctions
from bson.objectid import ObjectId
import copy
import json
import random

class Series(object):
    def __init__(self, series_columns, summation_column, collection, increment = None, filters={}):
        self.series_columns = series_columns
        self.summation_column = summation_column
        self.increment = increment
        self.filters = filters
        self.collection = collection

    def construct(self):
        (m, r) = getMapReduceFunctions(self.series_columns, self.summation_column, self.increment)
        map_reduce_results = getCollection(self.collection).inline_map_reduce(m, r, query=self.filters)
        if len(map_reduce_results) > 0:
            return self.normalize(map_reduce_results)
        else:
            return {}

    def normalize(self, serieses):
        result = {}
        series_data = []
        result['serieses'] = []
        timestamp_data = None
        for series in serieses:
            if series['_id'] == 'timestamps':
                timestamp_data = series
            else:
                series_data.append(series)
        unsorted_timestamps = []
        for key in timestamp_data['value']:
            unsorted_timestamps.append([timestamp_data['value'][key], key])
        
        unsorted_timestamps.sort()
        sorted_timestamps = []
        for ts in unsorted_timestamps:
            sorted_timestamps.append(ts[1])
        result['axis-label'] = sorted_timestamps
        
        minimum = 100000
        maximum = -100000
        
        for item in serieses:
            if item['_id'] != 'timestamps':
                series = {'name':item['_id'], 'data':[]}
                for ts in sorted_timestamps:
                    if ts in item['value']:
                        minimum = min(minimum, item['value'][ts])
                        maximum = max(maximum, item['value'][ts])
                        series['data'].append(item['value'][ts])
                    else:
                        series['data'].append(0)
                result['serieses'].append(series)
        result['max'] = maximum
        result['minimum'] = minimum
        return result
    
        