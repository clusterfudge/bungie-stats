import pymongo
from bson.code import Code
import os

collection = pymongo.Connection("c3p0.clusterfudge.org")['bungie-stats']['stats']
mapreduce_collection = pymongo.Connection("c3p0.clusterfudge.org")['bungie-stats']['mapout']

def getCollection(name):
	return pymongo.Connection("c3p0.clusterfudge.org")['bungie-stats'][name]

def load_function_text(function_name):
	f = open(os.getcwd() + '/mongo_functions/' + function_name + '.js', 'r+')
	text = f.read()
	f.close()
	return text

map_func = load_function_text('map')
reduce_func = load_function_text('reduce')


def getMapReduceFunctions(columns, summation, increment):
	mapFuncText = map_func.replace('%%COLUMNS%%', columns).replace('%%SUMMATION%%', summation).replace('%%INCREMENT%%', increment)
	reduceFuncText = reduce_func
	return Code(mapFuncText), Code(reduceFuncText)
