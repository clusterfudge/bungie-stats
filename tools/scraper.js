var username = "Dense Chester";


function getRowForName(table, name) {
  var rows = table..tr;
  for (var i = 0; i < rows.length(); i++) {
    if (rows[i]..span.length() > 0 && rows[i]..span..a.length() > 0 && rows[i]..span[0]..a[0].text() == name) {
      return rows[i];
    }
  }
  return {};
}

function printHeader() {
  print("gameId,playerName,gameType,gameDate,gameMap,playlist,place,kills,assists,deaths,kdspread,suicides,betrayals,score,weaponKills,grenadeKills,vehicleKills,otherKills,toolOfDestruction");
}
function getStats(username) {
var page = Web.getHtml('http://www.bungie.net/stats/playerstatshalo2.aspx?player=' + username.replace(" ", "+"));
var pageCount = page..div.(@['class'] = 'rgWrap rgInfoPart')..strong[1].text();
for ( var i = 1; i <= Number(pageCount); i++) {
  var url = "http://www.bungie.net/stats/playerstatshalo2.aspx?player=" + username.replace(" ", "+") + "&ctl00_mainContent_bnetpgl_recentgamesChangePage=" + i;
  var page = Web.getHtml(url);
  var rows = page..table.(@['class'] == "rgMasterTable").tbody..tr;
  for (var j = 0; j < rows.length(); j++) {
    var row = rows[j]..td;
    var gameLink = row[0].a.@href;
    var gameId = gameLink.substring(gameLink.indexOf("gameid=") + 7, gameLink.indexOf("&player"));
    var gameType = row[0].a.text();
    var gameDate = row[1];
    var gameMap = row[2];
    var playlist = row[3];
    var place = row[4];
    var gamePage = Web.getHtml("http://www.bungie.net" + gameLink);
    var statsTables = gamePage..table.(@['class'] == "stats");
    var carnageTable = statsTables[1];

    var carnageRow = getRowForName(carnageTable, username);
    var kills = carnageRow..td[1].text();
    var assists = carnageRow..td[2].text();
    var deaths = carnageRow..td[3].text();
    var kdspread = carnageRow..td[4].text();
    var suicides = carnageRow..td[5].text();
    var betrayals = carnageRow..td[6].text();
    var score = carnageRow..td[7].text();
    

    var weaponKills = "NULL";
    var meleeKills = "NULL";
    var grenadeKills = "NULL";
    var vehicleKills = "NULL";
    var otherKills = "NULL";
    var toolOfDestruction = "NULL";
    var headshots = "NULL";
    var bestSpree = "NULL";
    var avgLife = "NULL";
    var medals = "NULL";

    if (statsTables.length() ==4) {
      var breakdownTable = statsTables[2];
      var breakdownRow = getRowForName(breakdownTable, username);
      if (breakdownRow..td.length() < 7) {
        continue;
      }
      weaponKills = breakdownRow..td[1].text();
      meleeKills = breakdownRow..td[2].text();
      grenadeKills = breakdownRow..td[3].text();
      vehicleKills = breakdownRow..td[4].text();
      otherKills = breakdownRow..td[5].text();
      toolOfDestruction = breakdownRow..a.(@['class'] == "wep_kills_href").img.@alt

  }
  var r = [gameId, username, gameType, gameDate, gameMap, playlist, place,kills,assists,deaths,kdspread,suicides,betrayals,score,weaponKills,grenadeKills,vehicleKills,otherKills,toolOfDestruction];
    print(r.join(","));
    
  }

}
}
