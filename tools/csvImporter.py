from optparse import OptionParser
import json
import os
import pymongo



class Importer(object):
    def __init__(self):
        self.database = pymongo.Connection("c3p0.clusterfudge.org")['bungie-stats']

    def toMap(self, cols, line):
        ret = {}
        line = line.split(",")
        for i in range(len(cols)):
            if line[i] != "NULL":
                try:
                    ret[cols[i]] = int(line[i])
                except:
                    ret[cols[i]] = line[i]
                if cols[i] == 'gameDate':
                    parts = line[i].split(" ")[0].split("/")
                    day = int(parts[1])
                    month = int(parts[0])
                    year = int(parts[2])
                    ampm = line[i].split(" ")[2]
                    parts = line[i].split(" ")[1].split(":")
                    hour = int(parts[0])
                    if ampm == "PM":
                        hour += 12
                    ret['day'] = self.dateFormat(month, day, year, hour, 'day')
                    ret['month'] = self.dateFormat(month, day, year, hour, 'month')
                    ret['hour'] = self.dateFormat(month, day, year, hour, 'hour')
                    ret['year'] = year
                    ret['minute'] = int(parts[1])
                    ret['second'] = int(parts[2])
        return ret 
    
    def dateFormat(self, month, day, year, hour, increment):
        if increment == "month" or not increment:
            return str(month) + '/' + str(year)
        elif increment == "day":
            return str(month) + '/' + str(day) + '/' + str(year)
        elif increment == "hour":
            return str(month) + '/' + str(day) + '/' + str(year) + " " + str(hour) + ":00:00"
        else:
            return str(month) + '/' + str(year) #default to month

    def ingest(self, file):
        f = open(file, 'r')
        collection = self.database['stats-large']
        cols = f.readline().strip().split(",")
        for line in f:
            obj = self.toMap(cols, line.strip())
            #print json.dumps(obj)
            collection.save(obj)
        f.close()
    

if __name__ == '__main__':
    importer = Importer()
    for dirname, dirnames, filenames in os.walk('.'):
        for filename in filenames:
            if filename.endswith(".csv"):
                importer.ingest(os.path.join(dirname, filename))
    
    
    