import cherrypy as http
import json
from models.series import Series

class Controller(object):
    @http.expose
    def health_check(self):
        return "[OK]"
    
    @http.expose
    def graph(self, **params):
        series_member = params.get('series_member', 'playerName')
        if 'series_member' in params:
            del params['series_member']
        series_column = params.get('series_column', 'kills')
        if 'series_column' in params:
            del params['series_column']

        increment = params.get('increment', None)
        if 'increment' in params:
            del params['increment']
        
        collection = params.get('collection', 'stats')
        if 'collection' in params:
            del params['collection']

        for key in params:
            try:
                params[key] = int(params[key])
            except:
                continue
        
        if '_' in params:
            del params['_']
        series = Series(str(series_member), str(series_column), collection, str(increment), params)
        return json.dumps(series.construct())
        